from classes.comandament import Comandament
from classes.comandament_sony import ComandamentSony


def run_main():
    c_sony = ComandamentSony()
    c_sony.baixa_volum()
    c_sony.puja_volum()


if __name__ == '__main__':
    run_main()

