from abc import abstractmethod, ABC


# Sense definir abstractmethod, es classe abstracta informal
# De la manera en la que la tenim ara, es abstracta 100%, es la forma que te Python de fer ina interfaz

class Comandament(ABC):

    @abstractmethod
    def puja_volum(self):
        pass

    @abstractmethod
    def baixa_volum(self):
        pass

